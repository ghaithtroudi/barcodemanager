﻿using System;
using System.Collections.Generic;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace BarCodeScanner
{
    public partial class SellStock: Form
    {

        private List<Product> Products = DataDriver.Products;
        private List<ProductLine> ProductLines = new List<ProductLine>();
        private Boolean printed = false;

        public SellStock()
        {
            InitializeComponent();
            InitDataGrid();
        }

        private void barcodeBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                var text = barcodeTextBox.Text;
                if (text.Length > 3)
                {
                    var product = Products.Find(x => x.Barcode == text);
                    if (product == null)
                    {
                        MessageBox.Show("Cannot find Product, please Add Product before scanning barcode");
                        return;
                    }
                    var productLine = ProductLines.Find(x => x.Id == product.Id);
                    if (productLine != null)
                    {
                        productLine.Quantity++;
                    }
                    else
                    {
                        productLine = new ProductLine();
                        productLine.Quantity = 1;
                        productLine.Id = product.Id;
                        productLine.Name = product.Name;
                        productLine.Weight = product.Weight;
                        productLine.Stock = product.Stock;
                        productLine.Price = product.Price;
                        productLine.Barcode = product.Barcode;
                        productLine.Category = product.Category;
                        ProductLines.Add(productLine);
                    }
                    InitDataGrid();
                    barcodeTextBox.Text = "";
                    barcodeTextBox.Focus();
                    CalculateTotal();
                }
            }
        }
        private void InitDataGrid()
        {
            ProductLines = ProductLines.FindAll( x => x.Quantity > 0).OrderBy(x => x.Name).ToList();
            dataGridView.DataSource = typeof(List<ProductLine>);
            dataGridView.DataSource = ProductLines;
            ProductLines.OrderBy(x => x.Name.ToLower());
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.Columns["Name"].FillWeight = 400;
            dataGridView.Columns["Name"].ReadOnly = true;
            dataGridView.Columns["Barcode"].ReadOnly = true;
            dataGridView.Columns["Barcode"].FillWeight = 200;
            dataGridView.Columns["Category"].FillWeight = 100;
            dataGridView.Columns["Category"].ReadOnly = true;
            dataGridView.Columns["Quantity"].FillWeight = 100;
            dataGridView.Columns["Weight"].FillWeight = 100;
            dataGridView.Columns["Weight"].ReadOnly = true;
            dataGridView.Columns["Price"].FillWeight = 100;
            dataGridView.Columns["Price"].ReadOnly = true;
            dataGridView.Columns["Stock"].ReadOnly = true;
            dataGridView.Columns["Price"].HeaderText = "U.Price";
            dataGridView.Columns["Id"].Visible = false;
        }

        private string GetFullCAT(string cat)
        {
            switch (cat)
            {
                case "SUP": return "Super";
                case "ROY": return "Royal";
                case "1ERE": return "1er choix";
                case "NOR": return "Normal";
                case "2EME": return "2ème choix";
                case "SPL": return "Spécial";
                default: return "INCONNU";
            }
        }

        private void CalculateTotal()
        {
            var totalProducts = 0;
            decimal totalPrice = 0;
            if (ProductLines.Count > 0)
            {
                ProductLines.ForEach( product =>
                {
                    totalProducts += product.Quantity;
                    totalPrice += product.Quantity * product.Price;
                }
                );
                priceLabel.Text = "Total Price : " + totalPrice + " DT";
                numberLabel.Text = "Products : " + totalProducts;
            }
        }
        private void formShown(object sender, EventArgs e)
        {
            barcodeTextBox.Focus();
        }

        public void Print()
        {
            this.barcodeTextBox.Focus();
            this.printed = true;
            FileStream fs = new FileStream(Application.StartupPath + "\\printable.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
            Document doc = new Document(PageSize.A4, 24, 10, 24, 24);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            Paragraph p = new Paragraph(
                "Le " + DateTime.Now.ToString("dd/MM/yyyy"),
                FontFactory.GetFont("Calibri", 12, BaseColor.BLACK)
                );
            // p.Alignment = Element.ALIGN_RIGHT;
            doc.Add(p);
            p = new Paragraph("Facture Proformat / Client : " + clientNameBox.Text, FontFactory.GetFont("Calibri", 24, BaseColor.BLACK));
            doc.Add(p);

            decimal total = 0;
            int colis = 0;

            float[] widths = new float[] { 300f, 80f, 50f, 50f, 70f };
            // Table 1
            PdfPTable BigTable = new PdfPTable(5)
            {
                TotalWidth = 500f,
                LockedWidth = true,
                SpacingBefore = 20f,
                SpacingAfter = 20f
            };
            BigTable.SetWidths(widths);
            // BEGIN
            PdfPCell Property = new PdfPCell(new Phrase("Nom"));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("Cat."));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("Qté"));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("P.U"));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("P.Tot"));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);

            ProductLines.ForEach(product => {
                Property = new PdfPCell(new Phrase(product.Name));
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase(GetFullCAT(product.Category)));
                Property.HorizontalAlignment = Element.ALIGN_CENTER;
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase(product.Quantity.ToString()));
                Property.HorizontalAlignment = Element.ALIGN_CENTER;
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase(product.Price.ToString()));
                Property.HorizontalAlignment = Element.ALIGN_CENTER;
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase((product.Price * product.Quantity).ToString()));
                Property.HorizontalAlignment = Element.ALIGN_CENTER;
                BigTable.AddCell(Property);
                colis += product.Quantity;
                total += product.Price * product.Quantity;
            });

            doc.Add(BigTable);
            doc.Add(new Phrase("PRIX TOTAL : " + total.ToString() + " DT\n", FontFactory.GetFont("Calibri", 24, BaseColor.BLACK)));
            doc.Add(new Phrase("N° de Colis : " + colis.ToString() + " Colis", FontFactory.GetFont("Calibri", 24, BaseColor.BLACK))); ;

            doc.Close();

            // impression
            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo()
                {
                    CreateNoWindow = true,
                    Verb = "PrintTo",
                    FileName = Application.StartupPath + "\\printable.pdf" //put the correct path here
                }
            };
            proc.Start();
        }



        private void SaveStock()
        {
            if (ProductLines.Count > 0)
            {
                if (this.printed)
                {
                ProductLines.ForEach(
                    productLine =>
                    {
                        Products.Find(x => x.Id == productLine.Id).Stock -= productLine.Quantity;
                    });
                DataDriver.Save();
                this.Close();
                } else
                {
                    DialogResult dialogResult = MessageBox.Show("You saved without printing, do you want to print before saving ?", "WARNING !", MessageBoxButtons.YesNoCancel);
                    if (dialogResult == DialogResult.Yes)
                    {
                        Print();
                        ProductLines.ForEach(
                        productLine =>
                        {
                            Products.Find(x => x.Id == productLine.Id).Stock -= productLine.Quantity;
                        });
                        DataDriver.Save();
                        this.Close();
                    }
                    else if (dialogResult == DialogResult.No)
                    {                        ProductLines.ForEach(
                        productLine =>
                        {
                            Products.Find(x => x.Id == productLine.Id).Stock -= productLine.Quantity;
                        });
                        DataDriver.Save();
                        this.Close();
                    }
                    else if (dialogResult == DialogResult.Cancel)
                    {
                        return;
                    }
                }
            } else
            {
            this.Close();
            }
        }
        private void button_Click(object sender, EventArgs e)
        {
            SaveStock();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Print();
        }
    }
}
