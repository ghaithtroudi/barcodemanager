﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BarCodeScanner
{
    public class ProductLine
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Barcode { get; set; }
        public string Category { get; set; }
        public decimal Weight { get; set; }
        public int Stock { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
