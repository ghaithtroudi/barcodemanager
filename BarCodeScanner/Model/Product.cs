﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BarCodeScanner
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Barcode { get; set; }
        public string Category { get; set; }
        public int Stock { get; set; }
        public decimal Price { get; set; }
        public decimal Weight { get; set; }

        public Product(int Id, string Name, string Barcode, string Category)
        {
            this.Id = Id;
            this.Name = Name;
            this.Barcode = Barcode;
            this.Category = Category;
            this.Stock = 0;
            this.Price = 0;
            this.Weight = 0;
        }
    }
}
