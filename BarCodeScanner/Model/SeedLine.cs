﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BarCodeScanner
{
    public class SeedLine
    {
        public string Name { get; set; }
        public int Barcode { get; set; }
    }
}
