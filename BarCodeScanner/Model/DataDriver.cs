﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BarCodeScanner
{

    public static class DataDriver
    {

        public static List<Product> Products { get; set; }
        public static List<SeedLine> SeedLines { get; set; }
        public static void Save()
        {
            StreamWriter sw = null;
            using (sw = new StreamWriter(Application.StartupPath + @".\Products.json", false, Encoding.GetEncoding("iso-8859-1")))
                sw.Write(JsonConvert.SerializeObject(Products));
        }
        public static T ReadFromJson<T>(string file) where T : new()
        {
            if (File.Exists(file))
            {
                StreamReader sr = new StreamReader(File.OpenRead(file), Encoding.GetEncoding("iso-8859-1"));
                string json = sr.ReadToEnd();
                sr.Close();
                return JsonConvert.DeserializeObject<T>(json);
            }
            else return new T();
        }
        static DataDriver()
        {
            Products = ReadFromJson<List<Product>>(Application.StartupPath + @".\Products.json");
            SeedLines = ReadFromJson<List<SeedLine>>(Application.StartupPath + @".\Seed.json");

        }
    }
}
