﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BarCodeScanner
{
    public partial class Form1 : Form
    {

        public List<Product> Products;

        public Form1()
        {
            InitializeComponent();
            Products = DataDriver.Products.OrderBy(x => x.Name).ToList();
            dataGridView.DataSource = Products;
            InitDataGrid();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddProduct AddForm = new AddProduct();
            AddForm.Show(this);
        }

        private void InitDataGrid()
        {
            dataGridView.DataSource = typeof(List<Product>);
            dataGridView.DataSource = Products;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.Columns["Barcode"].ReadOnly = true;
            dataGridView.Columns["Name"].FillWeight = 400;
            dataGridView.Columns["Barcode"].FillWeight = 200;
            dataGridView.Columns["Category"].FillWeight = 100;
            dataGridView.Columns["Stock"].FillWeight = 100;
            dataGridView.Columns["Weight"].FillWeight = 100;
            dataGridView.Columns["Price"].FillWeight = 100;
            dataGridView.Columns["Id"].Visible = false;
            labelCount.Text = "Products : " +Products.Count.ToString();
            int stock = 0;
            decimal value = 0;
            Products.ForEach(
                product =>
                {   if (product.Stock > 0)
                    {
                    stock += product.Stock;
                    value += product.Stock * product.Price;
                    }
                });
            stockLabel.Text = "Stock : " + stock.ToString();
            valueLabel.Text = "Stock value : " + value.ToString() + " DT";
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            Products = DataDriver.Products.OrderBy(x => x.Name).ToList();
            InitDataGrid();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            DataDriver.Save();
        }

        private void StockButton_Click(object sender, EventArgs e)
        {
            AddStock AddStock = new AddStock();
            AddStock.Show(this);
        }

        private string GetFullCAT(string cat)
        {
            switch(cat)
            {
                case "SUP": return "Super";
                case "ROY": return "Royal";
                case "1ERE": return "1er choix";
                case "NOR": return "Normal";
                case "2EME": return "2ème choix";
                case "SPL": return "Spécial";
                case "A": return "A";
                case "B": return "B";
                default: return cat;
            }
        }

        private void SellButton_Click(object sender, EventArgs e)
        {
            SellStock SellStock = new SellStock();
            SellStock.Show(this);
        }

        private void filterBox_TextChanged(object sender, EventArgs e)
        {
            var text = filterBox.Text.Trim().ToLower();
            if (text.Length <= 2)
            {
                Products = DataDriver.Products.OrderBy(x => x.Name).ToList();
            } else {
            Products = Products.FindAll(x => x.Name.ToLower().Contains(text.ToLower()) || x.Barcode.ToLower().Contains(text.ToLower()));
            }
            dataGridView.DataSource = Products;
            InitDataGrid();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as CheckBox).Checked) {
                Products = Products.FindAll(x => x.Stock > 0);
            }
            else
            {
                Products = DataDriver.Products.OrderBy(x => x.Name).ToList();
            }
            InitDataGrid();
        }

        public void Print()
        {
            FileStream fs = new FileStream(Application.StartupPath + "\\printable.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
            Document doc = new Document(PageSize.A4, 24, 24, 24, 24);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();

            Paragraph p = new Paragraph("\n\n\nSTOCK", FontFactory.GetFont("Calibri", 24, BaseColor.BLACK));
            doc.Add(p);

            decimal total = 0;
            int colis = 0;

            float[] widths = new float[] { 300f, 80f, 50f, 50f, 70f };
            // Table 1
            PdfPTable BigTable = new PdfPTable(5)
            {
                TotalWidth = 500f,
                LockedWidth = true,
                SpacingBefore = 20f,
                SpacingAfter = 20f
            };
            BigTable.SetWidths(widths);
            // BEGIN
            PdfPCell Property = new PdfPCell(new Phrase("Nom"));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("Cat."));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("Qté"));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("P.U"));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("P.Tot"));
            Property.HorizontalAlignment = Element.ALIGN_CENTER;
            BigTable.AddCell(Property);

            Products.FindAll( x => x.Stock > 0).ForEach(product => {
                Property = new PdfPCell(new Phrase(product.Name));
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase(GetFullCAT(product.Category)));
                Property.HorizontalAlignment = Element.ALIGN_CENTER;
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase(product.Stock.ToString()));
                Property.HorizontalAlignment = Element.ALIGN_CENTER;
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase(product.Price.ToString()));
                Property.HorizontalAlignment = Element.ALIGN_CENTER;
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase((product.Price * product.Stock).ToString()));
                Property.HorizontalAlignment = Element.ALIGN_CENTER;
                BigTable.AddCell(Property);
                colis += product.Stock;
                total += product.Price * product.Stock;
            });

            doc.Add(BigTable);
            doc.Add(new Phrase("Total Value : " + total.ToString() + " DT\n", FontFactory.GetFont("Calibri", 18, BaseColor.BLACK)));
            doc.Add(new Phrase("Total products in stock : " + colis.ToString() + " Colis", FontFactory.GetFont("Calibri", 18, BaseColor.BLACK))); ;

            doc.Close();

            // impression
            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo()
                {
                    CreateNoWindow = true,
                    Verb = "PrintTo",
                    FileName = Application.StartupPath + "\\printable.pdf" //put the correct path here
                }
            };
            proc.Start();
        }



        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Print();
        }
    }
}
