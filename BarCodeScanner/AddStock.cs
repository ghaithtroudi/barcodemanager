﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BarCodeScanner
{
    public partial class AddStock : Form
    {

        private readonly List<Product> Products = DataDriver.Products;
        private List<ProductLine> ProductLines = new List<ProductLine>();
        private Boolean printed = false;

        public AddStock()
        {
            InitializeComponent();
            InitDataGrid();
        }

        private void BarcodeBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                var text = barcodeTextBox.Text;
                if (text.Length > 3)
                {
                    var product = Products.Find(x => x.Barcode == text);
                    if (product == null)
                    {
                        MessageBox.Show("Cannot find Product, please Add Product before scanning barcode");
                    } else
                    {
                    var productLine = ProductLines.Find(x => x.Id == product.Id);
                    if (productLine != null)
                    {
                        productLine.Quantity++;
                    }
                    else
                    {
                            productLine = new ProductLine
                            {
                                Quantity = 1,
                                Id = product.Id,
                                Name = product.Name,
                                Weight = product.Weight,
                                Barcode = product.Barcode,
                                Category = product.Category,
                                Price = product.Price
                            };
                            ProductLines.Add(productLine);
                    }
                    }
                    InitDataGrid();
                    barcodeTextBox.Text = "";
                    barcodeTextBox.Focus();
                    CalculateTotal();
                }
            }
        }
        private void InitDataGrid()
        {
            dataGridView.DataSource = typeof(List<ProductLine>);
            ProductLines = ProductLines.OrderBy(x => x.Name).ToList();
            dataGridView.DataSource = ProductLines;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.Columns["Barcode"].ReadOnly = true;
            dataGridView.Columns["Name"].FillWeight = 400;
            dataGridView.Columns["Barcode"].FillWeight = 200;
            dataGridView.Columns["Category"].FillWeight = 100;
            dataGridView.Columns["Quantity"].FillWeight = 100;
            dataGridView.Columns["Weight"].FillWeight = 100;
            dataGridView.Columns["Id"].Visible = false;
            dataGridView.Columns["Stock"].Visible = false;
        }
        private void FormShown(object sender, EventArgs e)
        {
            barcodeTextBox.Focus();
        }

        private void CalculateTotal()
        {
            var totalProducts = 0;
            decimal totalPrice = 0;
            if (ProductLines.Count > 0)
            {
                ProductLines.ForEach(product =>
                {
                    totalProducts += product.Quantity;
                    totalPrice += product.Quantity * product.Price;
                }
                );
                priceLabel.Text = "Total Price : " + totalPrice + " DT";
                numberLabel.Text = "Products : " + totalProducts;
            }
        }

        public void Print()
        {
            this.printed = true;
            FileStream fs = new FileStream(Application.StartupPath + "\\printable.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
            Document doc = new Document(PageSize.A4, 24, 24, 24, 24);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();

            Paragraph p = new Paragraph("\n\n\nBon de Livraison", FontFactory.GetFont("Calibri", 24, BaseColor.BLACK));
            doc.Add(p);

            decimal total = 0;
            int colis = 0;

            float[] widths = new float[] { 400f, 50f, 50f, 50f };
            // Table 1
            PdfPTable BigTable = new PdfPTable(4)
            {
                TotalWidth = 500f,
                LockedWidth = true,
                SpacingBefore = 20f,
                SpacingAfter = 20f
            };
            BigTable.SetWidths(widths);
            // BEGIN
            PdfPCell Property = new PdfPCell(new Phrase("Nom"))
            {
                HorizontalAlignment = Element.ALIGN_CENTER
            };
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("Qté"))
            {
                HorizontalAlignment = Element.ALIGN_CENTER
            };
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("P.U"))
            {
                HorizontalAlignment = Element.ALIGN_CENTER
            };
            BigTable.AddCell(Property);
            Property = new PdfPCell(new Phrase("P.Tot"))
            {
                HorizontalAlignment = Element.ALIGN_CENTER
            };
            BigTable.AddCell(Property);

            ProductLines.ForEach(product => {
                Property = new PdfPCell(new Phrase(product.Name));
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase(product.Quantity.ToString()))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase(product.Price.ToString()))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                BigTable.AddCell(Property);
                Property = new PdfPCell(new Phrase((product.Price * product.Quantity).ToString()))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                BigTable.AddCell(Property);
                colis += product.Quantity;
                total += product.Price * product.Quantity;
            });

            doc.Add(BigTable);
            doc.Add(new Phrase("PRIX TOTAL : " + total.ToString() + " DT\n", FontFactory.GetFont("Calibri", 24, BaseColor.BLACK)));
            doc.Add(new Phrase("N° de Colis : " + colis.ToString() + " Colis", FontFactory.GetFont("Calibri", 24, BaseColor.BLACK))); ;

            doc.Close();

            // impression
            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo()
                {
                    CreateNoWindow = true,
                    Verb = "PrintTo",
                    FileName = Application.StartupPath + "\\printable.pdf" //put the correct path here
                }
            };
            proc.Start();
        }



        private void SaveStock()
        {
            if (ProductLines.Count > 0)
            {
                if (this.printed)
                {
                    ProductLines.ForEach(
                        productLine =>
                        {
                            Products.Find(x => x.Id == productLine.Id).Stock -= productLine.Quantity;
                        });
                    DataDriver.Save();
                    this.Close();
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("You saved without printing, do you want to print before saving ?", "WARNING !", MessageBoxButtons.YesNoCancel);
                    if (dialogResult == DialogResult.Yes)
                    {
                        Print();
                        ProductLines.ForEach(
                        productLine =>
                        {
                            Products.Find(x => x.Id == productLine.Id).Stock += productLine.Quantity;
                        });
                        DataDriver.Save();
                        this.Close();
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        ProductLines.ForEach(
                           productLine =>
                           {
                               Products.Find(x => x.Id == productLine.Id).Stock += productLine.Quantity;
                           });
                        DataDriver.Save();
                        this.Close();
                    }
                    else if (dialogResult == DialogResult.Cancel)
                    {
                        return;
                    }
                }
            }
            else
            {
                this.Close();
            }
        }
        private void Button_Click(object sender, EventArgs e)
        {
            SaveStock();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            Print();
        }
    }
}
