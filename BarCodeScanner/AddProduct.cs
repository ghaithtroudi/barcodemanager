﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BarCodeScanner
{
    public partial class AddProduct : Form
    {
        public AddProduct()
        {
            InitializeComponent();
        }

        private void SaveProduct()
        {
            var count = DataDriver.Products.Last().Id +1;
            var Name = nameTextBox.Text;
            var Barcode = barcodeTextBox.Text;
            if (Name.Trim().Length < 2 || Barcode.Trim().Length < 2 )
            {
                MessageBox.Show("Please Fill all data");
                return;
            }
            var a = new Product(count, Name + " - ROYAL", Barcode+"-ROY", "ROY" );
            DataDriver.Products.Add(a);
            var b = new Product(count+1, Name + " - SUPER", Barcode + "-SUP", "SUP");
            DataDriver.Products.Add(b);
            var c = new Product(count+2, Name + " - 1ER CHOIX", Barcode + "-1ERE", "1ERE");
            DataDriver.Products.Add(c);
            var d = new Product(count+3, Name + " - NORMAL", Barcode + "-NOR", "NOR");
            DataDriver.Products.Add(d);
            var e = new Product(count+4, Name + " - 2EME CHOIX", Barcode + "-2EME", "2EME");
            DataDriver.Products.Add(e);
            var f = new Product(count + 5, Name + " - SPECIAL", Barcode + "-SPL", "SPL");
            DataDriver.Products.Add(f);
            var g = new Product(count + 6, Name + " - A", Barcode + "-A", "A");
            DataDriver.Products.Add(g);
            var h = new Product(count + 7, Name + " - B", Barcode + "-B", "B");
            DataDriver.Products.Add(h);
            DataDriver.Save();
            this.Close();
        }

        private void SeedProduct(SeedLine line)
        {
            var count = 0;
            if (DataDriver.Products.Count > 0)
            {
            count = DataDriver.Products.Last().Id + 1;
            }
            Name = line.Name;
            var code = "";
            for ( int i = 0; i< (5 - line.Barcode.ToString().Length); i++ )
            {
                code += "0";
            }
            code += line.Barcode.ToString();
            var a = new Product(count, Name + " - ROYAL", code + "-ROY", "ROY");
            DataDriver.Products.Add(a);
            var b = new Product(count + 1, Name + " - SUPER", code + "-SUP", "SUP");
            DataDriver.Products.Add(b);
            var c = new Product(count + 2, Name + " - 1ER CHOIX", code + "-1ERE", "1ERE");
            DataDriver.Products.Add(c);
            var d = new Product(count + 3, Name + " - NORMAL", code + "-NOR", "NOR");
            DataDriver.Products.Add(d);
            var e = new Product(count + 4, Name + " - 2EME CHOIX", code + "-2EME", "2EME");
            DataDriver.Products.Add(e);
            var f = new Product(count + 5, Name + " - SPECIAL", code + "-SPL", "SPL");
            DataDriver.Products.Add(f);
            var g = new Product(count + 6, Name + " - A", code + "-A", "A");
            DataDriver.Products.Add(g);
            var h = new Product(count + 7, Name + " - B", code + "-B", "B");
            DataDriver.Products.Add(h);
            DataDriver.Save();
        }


        private void SeedALLProducts()
        {
            DataDriver.SeedLines.ForEach(
                seedLine =>
                {
                    SeedProduct(seedLine);
                });
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // SeedALLProducts();
            SaveProduct();
        }
    }
}
